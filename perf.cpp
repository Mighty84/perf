#include <iostream>
#include <iomanip>
#include <regex>
#include <vector>
#include <string>
#include <cassert>
#include <Poco/Foundation.h>
#include <Poco/FileStream.h>
#include <Poco/RegularExpression.h>
#include <Poco/DirectoryIterator.h>
#include <Poco/DateTimeFormatter.h>
#include <Poco/DateTime.h>
#include <Poco/LocalDateTime.h>
#include <Poco/Thread.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>

struct MappedMemoryItem {
	std::string name;
	std::string privileges;
	
	uint64_t size;
	uint64_t pss;
	uint64_t sharedClean;
	uint64_t sharedDirty;
};

struct FileSystemItem {
	std::string path;
	uint64_t size;
	bool isExecutable;
	float blockRatio;
};

struct DiskStat {
	std::string name;
	uint64_t readSectors;
	uint64_t writtenSectors;
};

struct Partitions {
	std::string name;
	uint64_t blocks;
};

struct Application {
	Application() {
		pid = 0;
		utime = 0;
		stime = 0;
		startupTicks = 0;
		vsize = 0;
		rss = 0;
	}
	
	int32_t pid;
	uint64_t utime;
	uint64_t stime;
	uint64_t startupTicks;
	uint64_t vsize;
	uint64_t rss;

	std::string path;
	std::vector<std::shared_ptr<MappedMemoryItem>> mappedMemoryItems;
	std::vector<std::shared_ptr<FileSystemItem>> fileSystemItems;
};

struct SharedMappedMemoryMapping {
	std::shared_ptr<MappedMemoryItem> mappedMemoryItem;
	std::vector<std::shared_ptr<Application>> applications;
};

struct System {
	System() {
		uptimeTicks = 0;
		threads = 0;
	}
	
	uint64_t uptimeTicks;
	uint16_t threads;
	std::vector<std::shared_ptr<DiskStat>> diskStats;
	std::vector<std::shared_ptr<Partitions>> partitions;
	std::vector<Application> applications;
	std::vector<std::shared_ptr<SharedMappedMemoryMapping>> sharedMappedMemoryMappings;
};

int applicationReadSMaps(Application& application)
{
	std::cout << "\x1B[32mSMapsFile\x1B[0m" << std::endl;
	std::string smapsFile = "/proc/" + std::to_string(application.pid) + "/smaps";

	// Regular expression for item address, access priviledges and name
	uint8_t mandatoryMatchesItemExpression = 2+1;
	std::string itemExpression = "^[A-Fa-f0-9]+-[A-Fa-f0-9]+ ([rwxp-]+) [A-Fa-f0-9]+ [0-9:]+ [0-9:]+\\s+(.*)";
	std::regex itemRegEx(itemExpression);

	// Regular expression for item size
	uint8_t mandatoryMatchesSizeExpression = 1+1;
	std::string sizeExpression = "^Size:\\s+([0-9]+) kB";
	std::regex sizeRegEx(sizeExpression);

	// Regular expression for item proportional set size (PSS)
	uint8_t mandatoryMatchesPssExpression = 1+1;
	std::string pssExpression = "^Pss:\\s+([0-9]+) kB";
	std::regex pssRegEx(pssExpression);

	// Regular expression for item shared clean memory
	uint8_t mandatoryMatchesSharedCleanExpression = 1+1;
	std::string sharedCleanExpression = "^Shared_Clean:\\s+([0-9]+) kB";
	std::regex sharedCleanRegEx(sharedCleanExpression);

	// Regular expression for item shared dirty memory
	uint8_t mandatoryMatchesSharedDirtyExpression = 1+1;
	std::string sharedDirtyExpression = "^Shared_Dirty:\\s+([0-9]+) kB";
	std::regex sharedDirtyRegEx(sharedDirtyExpression);
	
	// Open the smaps file of the specified process
	Poco::FileInputStream fis(smapsFile);
	assert(fis.good());
	
	// Initial item
	std::shared_ptr<MappedMemoryItem> mmi = std::make_shared<MappedMemoryItem>();
	
	// Summary
	uint64_t totalSize = 0;
	uint64_t totalPss = 0;
	uint64_t totalSharedClean = 0;
	uint64_t totalSharedDirty = 0;
	
	// Parse the items from file line-by-line
	std::string read;
	while(std::getline(fis, read)) {
		// Read item name and access priviledges
		// We can't use the virtual address for anything really
		std::smatch itemMatches;
		std::regex_search(read, itemMatches, itemRegEx);
		
		if(itemMatches.size() > 0) {

			assert(itemMatches.size() == mandatoryMatchesItemExpression);
			std::cout << "Item: " << itemMatches[2].str() << std::endl;
			std::cout << "Privileges: " << itemMatches[1].str() << std::endl;
			
			// Store result
			mmi->name = itemMatches[2].str();
			mmi->privileges = itemMatches[1].str();
		}
		
		// Read item size
		std::smatch sizeMatches;
		std::regex_search(read, sizeMatches, sizeRegEx);
		
		if(sizeMatches.size() > 0) {
			assert(sizeMatches.size() == mandatoryMatchesSizeExpression);
			std::cout << "Size: " << sizeMatches[1].str() << " kB" << std::endl;
			
			// Store result
			mmi->size = std::stoll(sizeMatches[1].str());
			totalSize += mmi->size;
		}

		// Read proportional set-size
		std::smatch pssMatches;
		std::regex_search(read, pssMatches, pssRegEx);
		
		if(pssMatches.size() > 0) {
			assert(pssMatches.size() == mandatoryMatchesPssExpression);
			std::cout << "Pss: " << pssMatches[1].str() << " kB" << std::endl;
			
			// Store result
			mmi->pss = std::stoll(pssMatches[1].str());
			totalPss += mmi->pss;
		}

		// Read shared-clean
		std::smatch sharedCleanMatches;
		std::regex_search(read, sharedCleanMatches, sharedCleanRegEx);
		
		if(sharedCleanMatches.size() > 0) {
			assert(sharedCleanMatches.size() == mandatoryMatchesSharedCleanExpression);
			std::cout << "Shared_Clean: " << sharedCleanMatches[1].str() << " kB" << std::endl;
			
			// Store result
			mmi->sharedClean = std::stoll(sharedCleanMatches[1].str());
			totalSharedClean += mmi->sharedClean;
		}
		
		// Read shared-dirty
		std::smatch sharedDirtyMatches;
		std::regex_search(read, sharedDirtyMatches, sharedDirtyRegEx);
		
		if(sharedDirtyMatches.size() > 0) {
			assert(sharedDirtyMatches.size() == mandatoryMatchesSharedDirtyExpression);
			std::cout << "Shared_Dirty: " << sharedDirtyMatches[1].str() << " kB" << std::endl;
			
			// Store result
			mmi->sharedDirty = std::stoll(sharedDirtyMatches[1].str());
			totalSharedDirty += mmi->sharedDirty;
			
			// Received all the information from this item
			// Store the result and create a new one
			application.mappedMemoryItems.push_back(mmi);
			mmi = std::make_shared<MappedMemoryItem>();
			
			std::cout << std::endl;
		}
	}
	
	// Show a summary
	std::cout << "Total memory size: " << totalSize << " kB" << std::endl;
	std::cout << "Total memory proportional set size (PSS): " << totalPss << " kB" << std::endl;
	std::cout << "Total memory shared clean: " << totalSharedClean << " kB" << std::endl;
	std::cout << "Total memory shared dirty: " << totalSharedDirty << " kB" << std::endl;
	
	// Close the smaps file
	fis.close();

	std::cout << std::endl;
	return 0;
}

static int recurseDirectory(Application& application, const std::string& path)
{
	Poco::DirectoryIterator end;
	
	for (Poco::DirectoryIterator it(path); it != end; ++it) {

		std::cout << "File: " << it->path() << std::endl;
		std::cout << "FileSize: " << it->getSize() << " B" << std::endl;

		std::cout << "Privileges: ";
		std::cout << (it->isDirectory() ? "d" : "-");
		std::cout << (it->canRead()     ? "r" : "-");
		std::cout << (it->canWrite()    ? "w" : "-");
		std::cout << (it->canExecute()  ? "x" : "-");
		std::cout << std::endl;
		
		if(it->isDirectory())
			recurseDirectory(application, it->path());
		else {
			// Use fstat as Poco does not supply what we need
			struct stat fst;
			int fildes = open(it->path().c_str(), O_RDONLY);
			int status = fstat(fildes, &fst);
			std::cout << "Filesize: " << fst.st_size << " B" << std::endl;
			std::cout << "Blocks: " << fst.st_blocks << std::endl;
			std::cout << "Blocksize: " << fst.st_blksize << " B" << std::endl;
			
			float blockRatio = (fst.st_size / (float)(fst.st_blocks*fst.st_blksize)) * 100.0f;
			std::cout << std::setprecision(3) << "BlockRatio: " << blockRatio << "%" << std::endl;

			std::shared_ptr<FileSystemItem> fsi = std::make_shared<FileSystemItem>();
			fsi->path = it->path();
			fsi->size = fst.st_size;
			fsi->isExecutable = it->canExecute();
			fsi->blockRatio = blockRatio;
			
			// Store result
			application.fileSystemItems.push_back(fsi);
		}
	}
	
	return 0;
}

 void applicationReadDirectory(Application& application)
{
	std::cout << "\x1B[32mParseDirectory\x1B[0m" << std::endl;
	const std::string & path = application.path;
	recurseDirectory(application, path);
	
	std::cout << std::endl;
}

void applicationReadStat(Application& application)
{
	std::cout << "\x1B[32mReadStat\x1B[0m" << std::endl;
	std::string statFile = "/proc/" + std::to_string(application.pid) + "/stat";
	
	// Regular expression for utime, stime, startup
	uint8_t mandatoryMatchesStatExpression = 7+1;
	std::string statExpression = "^\\d+ \\(.*\\) \\w [-]?\\d+"
		" [-]?\\d+ [-]?\\d+ [-]?\\d+ [-]?\\d+"
		" \\d+ \\d+ \\d+ \\d+ \\d+"
		" (\\d+) (\\d+) (\\d+) (\\d+)"
		" [-]?\\d+ [-]?\\d+ [-]?\\d+ [-]?\\d+"
		" (\\d+) (\\d+) (\\d+) .*";
	std::regex statRegEx(statExpression);
	
	// Open the stat file of the specified process or thread
	Poco::FileInputStream fis(statFile);
	assert(fis.good());
	
	// Parse the items from file line-by-line
	std::string read;
	while(std::getline(fis, read)) {
		// Read stats
		std::smatch statMatches;
		std::regex_search(read, statMatches, statRegEx);

		assert(statMatches.size() == mandatoryMatchesStatExpression);
			
		uint64_t utime = std::stoll(statMatches[1].str()) + std::stoll(statMatches[2].str());
		uint64_t stime = std::stoll(statMatches[3].str()) + std::stoll(statMatches[4].str());
		uint64_t startupTicks = std::stoll(statMatches[5].str());
		uint64_t vsize = std::stoll(statMatches[6].str()) / 1024; // preferred in kB
		uint64_t rss = std::stoll(statMatches[7].str()) * getpagesize() / 1024; // RSS in kB not pages
		
		std::cout << "Ticks in user-mode (utime + cutime): " << utime << std::endl;
		std::cout << "Ticks in kernel-mode (stime + cstime): " << stime << std::endl;
		std::cout << "Ticks since start (starttime): " << startupTicks << std::endl;
		std::cout << "Virtual memory size: " << vsize << " kB" << std::endl;
		std::cout << "Resident set size (RSS): " << rss << " kB" << std::endl;

		// Store result
		// Also store the time when the sample was obtained
		application.utime = utime;
		application.stime = stime;
		application.startupTicks = startupTicks;
		application.vsize = vsize;
		application.rss = rss;
	}
	
	// Close the stat file
	fis.close();
	
	std::cout << std::endl;
}

void systemReadUptime(System& system) 
{
	std::cout << "\x1B[32mReadUptime\x1B[0m" << std::endl;

	std::string uptimeFile = "/proc/uptime";
	
	// Regular expression for uptime
	uint8_t mandatoryMatchesUptimeExpression = 1+1;
	std::string uptimeExpression = "^([0-9]+\\.?[0-9]*) [0-9]+\\.?[0-9]*";
	std::regex uptimeRegEx(uptimeExpression);
	
	// Open the uptime file
	Poco::FileInputStream fis(uptimeFile);
	assert(fis.good());
	
	// Parse the items from file line-by-line
	std::string read;
	while(std::getline(fis, read)) {
		// Read uptime
		std::smatch uptimeMatches;
		std::regex_search(read, uptimeMatches, uptimeRegEx);
		
		assert(uptimeMatches.size() == mandatoryMatchesUptimeExpression);

		long ticks_per_second = sysconf(_SC_CLK_TCK);
		uint64_t uptimeTicks = (uint64_t) (std::stod(uptimeMatches[1].str()) * ticks_per_second);
		
		std::cout << "System uptime in ticks: " << uptimeTicks << std::endl;

		// Store result
		system.uptimeTicks = uptimeTicks;
	}
	
	// Close the stat file
	fis.close();
	
	std::cout << std::endl;
}

void applicationReadTaskCount(Application& application)
{
	std::cout << "\x1B[32mTaskCount\x1B[0m" << std::endl;
	std::string taskDirectory = "/proc/" + std::to_string(application.pid) + "/task";
	
	Poco::DirectoryIterator end;
	uint32_t count = 0;
	
	for (Poco::DirectoryIterator it(taskDirectory); it != end; ++it)
		++count;
		
	std::cout << "Number of tasks in process (including self): " << count << std::endl;
	std::cout << std::endl;
}

void applicationReadStorageAccess(Application& application) 
{
	std::cout << "\x1B[32mStorageAccess\x1B[0m" << std::endl;
	std::string ioFile = "/proc/" + std::to_string(application.pid) + "/io";
	
	// Open the io file of the specified process or thread
	Poco::FileInputStream fis(ioFile);
	assert(fis.good());
	
	// Parse the items from file line-by-line
	std::string read;
	while(std::getline(fis, read)) {
		// Regular expression for bytes read from physical storage
		uint8_t mandatoryMatchesReadBytesExpression = 1+1;
		std::string readBytesExpression = "^syscr: (\\d+)";
		std::regex readBytesRegEx(readBytesExpression);
		
		// Read readBytes
		std::smatch readBytesMatches;
		std::regex_search(read, readBytesMatches, readBytesRegEx);

		if(readBytesMatches.size() == mandatoryMatchesReadBytesExpression) {
			std::cout << "Read bytes from physical storage (uncached, without memmap): " << readBytesMatches[1].str() << std::endl;
			uint64_t readBytes = std::stoll(readBytesMatches[1].str());
		}

		// Regular expression for bytes written to physical storage
		uint8_t mandatoryMatchesWroteBytesExpression = 1+1;
		std::string wroteBytesExpression = "^syscw: (\\d+)";
		std::regex wroteBytesRegEx(wroteBytesExpression);
		
		// Read wroteBytes
		std::smatch wroteBytesMatches;
		std::regex_search(read, wroteBytesMatches, wroteBytesRegEx);
		
		if(wroteBytesMatches.size() == mandatoryMatchesWroteBytesExpression) {
			std::cout << "Written bytes to physical storage (uncached, without memmap): " << wroteBytesMatches[1].str() << std::endl;
			uint64_t wroteBytes = std::stoll(wroteBytesMatches[1].str());
		}
	}
	
	// Close the io file
	fis.close();
	
	std::cout << std::endl;
}

void systemReadThreadCount(System& system)
{
	std::cout << "\x1B[32mThreadCount\x1B[0m" << std::endl;
	std::string cpuInfoFile = "/proc/cpuinfo";
	
	// Open the cpuinfo file of the specified process or thread
	Poco::FileInputStream fis(cpuInfoFile);
	assert(fis.good());
	
	uint16_t processors;
	
	// Parse the items from file line-by-line
	std::string read;
	while(std::getline(fis, read)) {
		// Regular expression for processors
		uint8_t mandatoryMatchesProcessorsExpression = 1+1;
		std::string processorsExpression = "^processor\\t: (\\d+)";
		std::regex processorsRegEx(processorsExpression);
		
		// Read processors
		std::smatch processorsMatches;
		std::regex_search(read, processorsMatches, processorsRegEx);

		if(processorsMatches.size() == mandatoryMatchesProcessorsExpression)
			processors = std::stoi(processorsMatches[1].str())+1;
	}
	
	// The last processor line would be the number of
	// CPU threads (not fully fledged cores). We are actually
	// interested in the number of threads available for parallel task execution
	std::cout << "Threads: " << processors << std::endl;
	
	// Store result
	system.threads = processors;
	
	std::cout << std::endl;
}

void systemReadLoadAverage(System& system) {
	std::cout << "\x1B[32mLoadAverage\x1B[0m" << std::endl;
	std::string cpuInfoFile = "/proc/loadavg";
	
	// Open the loadavg file
	Poco::FileInputStream fis(cpuInfoFile);
	assert(fis.good());
	
	// Parse the items from file line-by-line
	std::string read;
	while(std::getline(fis, read)) {
		// Regular expression for loadavg
		uint8_t mandatoryMatchesLoadAvgExpression = 3+1;
		std::string loadAvgExpression = "^([0-9]+\\.?[0-9]*) ([0-9]+\\.?[0-9]*) ([0-9]+\\.?[0-9]*) .*";
		std::regex loadAvgRegEx(loadAvgExpression);
		
		// Read loadavg
		std::smatch loadAvgMatches;
		std::regex_search(read, loadAvgMatches, loadAvgRegEx);
		
		assert(loadAvgMatches.size() == mandatoryMatchesLoadAvgExpression);
		float load1m = std::stof(loadAvgMatches[1].str());
		float load5m = std::stof(loadAvgMatches[2].str());
		float load15m = std::stof(loadAvgMatches[3].str());
		
		// We need to know the number of threads available first
		assert(system.threads != 0);
		
		// Calculate and display loads considering all threads (processors)
		std::cout << "Load 1m: " << load1m 
			<< " or: " << std::setprecision(2) << (load1m/(float)system.threads)*100.0f << "%" << std::endl;
		std::cout << "Load 5m: " << load5m
			<< " or: " << std::setprecision(2) << (load5m/(float)system.threads)*100.0f << "%" << std::endl;
		std::cout << "Load 15m: " << load15m
			<< " or: " << std::setprecision(2) << (load15m/(float)system.threads)*100.0f << "%" << std::endl;
	}
	
	std::cout << std::endl;
}

void systemReadDiskStats(System& system)
{
	std::cout << "\x1B[32mReadDiskStats\x1B[0m" << std::endl;
	std::string cpuInfoFile = "/proc/diskstats";
	
	// Open the diskstats file
	Poco::FileInputStream fis(cpuInfoFile);
	assert(fis.good());
	
	std::shared_ptr<DiskStat> ds = std::make_shared<DiskStat>();
	
	// Parse the items from file line-by-line
	std::string read;
	while(std::getline(fis, read)) {
		// Regular expression for every diskstats entry
		uint8_t mandatoryMatchesDiskStatsExpression = 3+1;
		std::string diskStatsExpression = "^\\s+\\d+\\s+\\d+ ([a-z0-9-]+) \\d+ \\d+ (\\d+) \\d+ \\d+ \\d+ (\\d+) .*";
		std::regex diskStatsRegEx(diskStatsExpression);
		
		// Read diskstats
		std::smatch diskStatsMatches;
		std::regex_search(read, diskStatsMatches, diskStatsRegEx);
		
		assert(diskStatsMatches.size() == mandatoryMatchesDiskStatsExpression);
		std::string name = diskStatsMatches[1].str();
		uint64_t readSectors = std::stoll(diskStatsMatches[2].str());
		uint64_t writtenSectors = std::stoll(diskStatsMatches[3].str());
		
		std::cout << name << " read sectors: " << readSectors << std::endl;
		std::cout << name << " wrote sectors: " << writtenSectors << std::endl;
		
		// Store results
		ds->name = name;
		ds->readSectors = readSectors;
		ds->writtenSectors = writtenSectors;
		
		// Received all the information from this item
		// Store the result and create a new one
		system.diskStats.push_back(ds);
		ds = std::make_shared<DiskStat>();
	}
	
	std::cout << std::endl;
}

void systemReadPartitions(System& system)
{
	std::cout << "\x1B[32mReadPartitions\x1B[0m" << std::endl;
	std::string cpuInfoFile = "/proc/partitions";
	
	// Open the diskstats file
	Poco::FileInputStream fis(cpuInfoFile);
	assert(fis.good());
	
	std::shared_ptr<Partitions> pts = std::make_shared<Partitions>();
	
	// Parse the items from file line-by-line
	std::string read;
	while(std::getline(fis, read)) {

		// Regular expression for every partition
		uint8_t mandatoryMatchesPartitionsExpression = 2+1;
		std::string partitionsExpression = "^\\s+\\d+\\s+\\d+\\s+(\\d+) ([a-z0-9-]+)";
		std::regex partitionsRegEx(partitionsExpression);
		
		// Read partitions
		std::smatch partitionsMatches;
		std::regex_search(read, partitionsMatches, partitionsRegEx);
		
		if(partitionsMatches.size() == mandatoryMatchesPartitionsExpression) {
			uint64_t partitionsBlocks = std::stoll(partitionsMatches[1].str());
			std::string partitionName = partitionsMatches[2].str();
		
			std::cout << partitionName << " has " << partitionsBlocks << " blocks" << std::endl;
			
			// Store results
			pts->name = partitionName;
			pts->blocks = partitionsBlocks;
			
			// Received all the information from this item
			// Store the result and create a new one
			system.partitions.push_back(pts);
			pts = std::make_shared<Partitions>();
		}
	}
	
	std::cout << std::endl;
}


void systemAnalyzeCPUTime(System& system)
{
	// Last time we measured the uptime
	uint64_t prev_uptime_ticks = system.uptimeTicks;
	
	for(auto & application : system.applications) {

		std::cout << "\x1B[32mAnalyzeCPUTime (" << application.pid << ")\x1B[0m" << std::endl;

		// Get previous values
		uint64_t prev_utime = application.utime;
		uint64_t prev_stime = application.stime;
		
		// Reread uptime
		systemReadUptime(system);
		uint64_t uptimeTicks = system.uptimeTicks;
		
		// Reread application stats
		applicationReadStat(application);
		uint64_t utime = application.utime;
		uint64_t stime = application.stime;
		
		// Calculate/obtain intermediate values
		long ticks_per_second = sysconf(_SC_CLK_TCK);
		uint64_t ticks_since_application_start = uptimeTicks - application.startupTicks;
		uint64_t ticks_in_sample_time = uptimeTicks - prev_uptime_ticks;

		// Calculate the CPU load since application start
		// Consider all threads (processors)
		double timeSinceStartup = ticks_since_application_start / (double) ticks_per_second;
		double percentSinceStartup = (utime + stime)
			/ (double) ticks_since_application_start / system.threads * 100.0d;
			
		std::cout << "Load since application startup (" << timeSinceStartup << "s): " 
			<< std::setprecision(3) << percentSinceStartup << "%" << std::endl;

		// Calculate the CPU load since last sample
		// Consider all threads (processors)
		double timeDiff = ticks_in_sample_time / (double) ticks_per_second;
		double percentDiff = ((utime + stime) - (prev_utime + prev_stime))
			/ (double) ticks_in_sample_time / system.threads * 100.0d;
		
		std::cout << "Load since last calculation (" << timeDiff << "s): " 
			<< std::setprecision(3) << percentDiff << "%" << std::endl;
			
		std::cout << std::endl;
	}
}

void systemAnalyzeSharedMemory(System& system)
{
	std::cout << "\x1B[32mAnalyzeSharedMemory\x1B[0m" << std::endl;

	// Collect all of the clean and dirty shared mapped-memory-items from 
	// each application
	for(auto & application : system.applications) {

		for(auto const& mmi: application.mappedMemoryItems) {

			// Is a mappedMemoryItem shared clean or dirty
			// Then add a reference to the sharedMappedMemoryMapping list
			// and of the application using it
			if(mmi->sharedClean > 0 || mmi->sharedDirty > 0) {
								
				bool foundExisting = false;
				
				for(auto & smmm : system.sharedMappedMemoryMappings) {
				
					if(smmm->mappedMemoryItem->name == mmi->name
						&& smmm->mappedMemoryItem->size == mmi->size
						&& smmm->mappedMemoryItem->privileges == mmi->privileges) {
							std::shared_ptr<Application> app = std::make_shared<Application>(application);
							smmm->applications.push_back(app);

							foundExisting = true;
							break;
					}
				}

				if(!foundExisting) {
					std::shared_ptr<SharedMappedMemoryMapping> nsmmm = std::make_shared<SharedMappedMemoryMapping>();
					nsmmm->mappedMemoryItem = mmi;
					std::shared_ptr<Application> app = std::make_shared<Application>(application);
					nsmmm->applications.push_back(app);
					system.sharedMappedMemoryMappings.push_back(nsmmm);
				}
			}
		}
	}

	// Iterate through the list of sharedMappedMemoryMappings and output 
	// the name of the shared item and the application referencing it
	for(auto const& smmi : system.sharedMappedMemoryMappings) {
		std::string st = smmi->mappedMemoryItem->sharedClean ? "clean" : "dirty";
		std::cout << "Shared " << st << " mapping: " << smmi->mappedMemoryItem->name 
			<< " with size: " << smmi->mappedMemoryItem->size << " kB by: ";
			
		for(auto const& app : smmi->applications)
			std::cout << "(" << app->pid << ")";
		
		std::cout << std::endl;
	}
	
	// Now for every application, output the shared memory items it is referencing
	for(auto & application : system.applications) {
		
		uint32_t totalSharedAssigned = 0;
		
		for(auto const& smmi : system.sharedMappedMemoryMappings)
			for(auto const& app : smmi->applications)
				if(app->pid == application.pid) {
					uint16_t items = smmi->applications.size();
					uint32_t assigned = smmi->mappedMemoryItem->size/items;
					
					std::cout << application.pid 
						<< " takes " << 1/(float)items*100.0f << "%"
						<< " (" << assigned << " kB)"
						<< " of " << smmi->mappedMemoryItem->name << std::endl;
						
					totalSharedAssigned += assigned;
				}
				
		std::cout << application.pid << " utilized " << totalSharedAssigned << " kB" << std::endl;
	}
	std::cout << std::endl;
}

void systemReadAll(System& system)
{
	// System uptime in ticks
	systemReadUptime(system);
	
	// >>> PS
	// Return the number of CPU threads available for execution
	systemReadThreadCount(system);
	
	// Load average can be called periodically
	systemReadLoadAverage(system);

	// >>> DU
	// Read the number of available blocks per partition
	systemReadPartitions(system);
	
	// >>> BW
	// Return the number of blocks written to storage devices
	systemReadDiskStats(system);
}

void applicationReadAll(Application& application)
{
	// >>> LS/DM
	// Obtain all the shared and private owned mapped memory items
	applicationReadSMaps(application);
	
	// >>> DU/FS
	// Return the file size, blocks and device block-size
	// for the files owned by the application
	applicationReadDirectory(application);
	
	// >>> PS/DM
	// Return the number of ticks the process has executed since started
	// Sudden or sporadic changes in CPU load can only be derived by continuous measurements/updates
	applicationReadStat(application);
	// Return the number of tasks (thread) the process owns
	applicationReadTaskCount(application);
	
	// >>> IO
	// Obtain the I/O generated by the application
	// This does not differentiate what specific device was read/written and does
	// not provide any information about read/written blocks
	applicationReadStorageAccess(application);
}

void systemAnalyzeAll(System& system)
{
	systemAnalyzeCPUTime(system);
	systemAnalyzeSharedMemory(system);
}

int main() {
	// Obtain generic system information
	std::cout << "\x1B[41mSystem\x1B[0m" << std::endl;
	System system;
	systemReadAll(system);
	
	// Analyze an application
	Application application1;
	application1.pid = 96944;
	application1.path = "/opt/dex2jar";
	std::cout << "\x1B[41mApplication (" << application1.pid << ")\x1B[0m" << std::endl;
	applicationReadAll(application1);
	system.applications.push_back(application1);

	Application application2;
	application2.pid = 97070;
	application2.path = "/opt/dex2jar";
	std::cout << "\x1B[41mApplication (" << application2.pid << ")\x1B[0m" << std::endl;
	applicationReadAll(application2);
	system.applications.push_back(application2);
	
	// Short pause to show some effect on the 
	// proceeding calculations
	std::cout << "\x1B[43mSleep\x1B[0m" << std::endl;
	sleep(3);
	
	// Analyze specific data
	std::cout << "\x1B[41mAnalyze\x1B[0m" << std::endl;
	systemAnalyzeAll(system);
	return 0;
}
